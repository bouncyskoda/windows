<pre>
2022 Windows Server Checklist  (draft)
_____  Create a video using ffmpeg from Liunx showing most of the following:
         Post video in localhost/video/ Linux apache /var/www/html/video/
#WINDDOWS VM CHECKLIST (VIRTUAL BOX or VMWARE)
TRY TO USE 64 BIT APPLICATIONS
V Set VM to bridge.
V Set Static ip : Control Panel\Network and Internet\Network Connections
        AM 10.183.11.# / 16  PM 10.183.22.# / 16
V Create a "windows" project(repo) in your gitlab.com account.
V Add the files from this project(repo). https://gitlab.com/tritechsc/windows
V Install MS Visual Studio Code (https://code.visualstudio.com/)
V Install Python 64 bit.  Test with your turtle graphics. C:\Program Files\Python39>
V Process explorer - upload text output to data folder.
V Install and run tcpview - upload text output to the windows project
V Change the name of your computer (hostname)
V Install 2 pdf viewers (Adobe Acrobat Reader  and your choice)
V Install firefox and chrome
V Install and run Packettracer 
V Install and use git ( https://git-scm.com/downloads)
        Make git usable with PowerShell
_____ Install and use atom.io (optional)
V Install and use putty (64bit)
V Install and use cygwin
V Configure Cygwin for ssh gcc, g++ and ssh)
V Configure Cygwin to compile c code.
V Compile and C code using Powershell 
V Install JDK from Oracle 
_____  Configure classpath for JDK. (Deprecated)
V  Compile and run JAVA,
V  Enable God Mode,TaskBarIcons,MyComputer,WinVault,Firewall,Network and NetworkedProgrammInstall
Look at the following using file explorer: %appdata%  and %localappdata%
V Upload a screenshots of the result of %appdata% to gitlab data
V Upload a screenshots of the result of %localappdata% to gitlab data
V compmgmt.msc (Screenshot compmgmt.msc showing users and a custom group.)
V mmc (Screenshot host mmc with 3 windows users with one user in a custom group.)
V Create folder on your desktop and share it.  Have someone add files to that folder.
V  Install BGInfo.zip and appy the settings to your desktop. (https://docs.microsoft.com/en-us/sysinternals/downloads/bginfo)
V Enable OpenSSH and OpenSSH Server 
          Windows-Settings>Apps>
           Manage optional features
           +Add a feature
            (Add OpenSSH Client and OpenSSH Server)
############################################################################
Server client process:
V Install fileZilla and connect to an ftp server.
        Probably 10.183.42.42
        Download irfanview (iview.7z) and install it.(optional)
Server side processes:
V Install one copy of Nginx.  Install the package at c:\nginx
V Add a custom html to Nginx document root.
V Start a python webserver at ip:8000
V Place text and images in a directory "web/files"
        python -m http.server -d web 8000
V Link the python server from the nginx server index.html page. <a href = "http://10.183.**.**:8000" target = "_blank" > port:8000</a>
        Notes: https://stackoverflow.com/questions/39801718/how-to-run-a-http-server-which-serves-a-specific-path
</pre>
